package core;

import java.util.Arrays;

public class prueba {

	static int[] ints = {-9, 14, 37, 102};
	
	public static void main(String[] args) {
		
		long startTime = System.nanoTime();
		System.out.println("Search");
		System.out.println(exists(ints, 36));
		System.out.println(exists(ints, 102));
		long endTime = System.nanoTime();
		System.out.println("timed: "+(startTime-endTime));
		
		startTime = System.nanoTime();
		System.out.println("binarySearch");
		System.out.println(binarySearch(ints, 36));
		System.out.println(binarySearch(ints, 102));
		endTime = System.nanoTime();
		System.out.println("timed: "+(startTime-endTime));
	}
	
	static boolean exists(int[] ints, int k) {
		boolean respuesta =false;
		
		for (int i = 0; i < ints.length; i++) {
			
			//ascending arranged
			if(ints[i] > k){
				break;
			}
			
			
			if(ints[i]== k){
				respuesta = true;
				break;
			}
		}
		
		return respuesta;
		
	}
	
	
	static boolean binarySearch(int[] ints, int k){
		return Arrays.binarySearch(ints, k) >=0 ;
	}
	
}
