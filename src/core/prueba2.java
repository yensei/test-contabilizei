package core;

/*
Today, stores are increasingly equipped with automated checkout machines. Most of these machines accept payment only by credit card, despite the fact that a significant portion of 
consumers continue to pay for goods with cash.

One of the problems presented by cash transactions is how to return change: what is the optimal way to give back a certain amount, with the minimum number of coins and bills? 
Its an issue that each of us encounters on a daily basis and the problem is the same for automated checkout machines.

In this exercise, you are asked to try and find an optimal solution for returning change in a very specific case: when the machine contains only 2 coins, 5 bills and 10 bills.
To simplify the problem, we imagine that all of these coins and bills are available in unlimited quantities.

Here are some examples of how change may be returned:
Change for				Possible Solutions 			Optimal Solution 
1 						Impossible					Impossible
6						2 + 2 + 2				2 + 2 + 2
10						2 + 2 + 2 +2 + 2		5 + 5
10						10							
9223372036854775807	...							(10 * 922337203685477580) + 5 + 2

Change is represented as a Change object. This object has three properties: coin2, bill5 and bill10 which represents the numbers of 2 coins, 5 bills and 10 bills.

For instance, when applied to example #2 of the table above (6), we should get a Change object with the following values:
coin2 value is 3 (3 2 coins)
bill5 value is 0 (no 5 bill)
bill10 value is 0 (no 10 bill)
Implement the optimalChange(long s) method which returns a Change object. The sum of coins and bills indicated in the Change object must be s. If it is not possible to give back change  as in example #1 , the method must return null.

To get a maximum number of points, your solution should always provide a result  when possible  having the minimal number of bills and coins.

Constraints:
s is a long.
0 < s <= 9223372036854775807
 
 */
public class prueba2 {
	
	public static void main(String[] args) {
		long s = 27L; // Change this value to perform other tests
		System.out.println("Paid     s: " + s+"\n");
		Change m = Solution.optimalChange(s);
		if(m!=null){
			long result = m.coin2 * 2 + m.bill5 * 5 + m.bill10 * 10;
			System.out.println("Coin(s)  2: " + m.coin2);
			System.out.println("Bill(s)  5: " + m.bill5);
			System.out.println("Bill(s) 10: " + m.bill10);
			System.out.println("\nChange given = " + result);
		}
	}
}
//Do not modify Change
class Change {
 long coin2 = 0;
 long bill5 = 0;
 long bill10 = 0;

}

class Solution {
	static Change change;
	static long rest;
	// Do not modify this method signature
	 static Change optimalChange(long s) {
		change = new Change();
		 rest = s;
		
		try {
			calculate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			change = null;
		}
				 
		return 	change;	 
		      
	 }
	 
	 
	 static long calculate() throws Exception{
		 
		 if(rest >= 10){
			 
			 change.bill10 = rest/10;
			 rest = rest%10;
			 
			 if(rest >= 10 && rest > 0){
				 calculate();
			 }
		 }
		 
		 
		 //Si es multiplo de 5
		 if(rest%5 == 0){ 
			 if(rest >= 5){
				 rest -= 5;
				 change.bill5 +=1;
				 
				 if(rest >= 5 && rest > 0){
					 calculate();
				 }
			 }
		 }
		 
		 
		 if(rest%2 == 0 || rest > 2){
			 if(rest >= 2){
				 rest -= 2;
				 change.coin2 +=1;
				 
				 if(rest >= 2 && rest > 0){
					 calculate();
				 }
			 }
		 }

		 if( (rest < 2 && rest >0)  || rest < 0 ){
//			 System.out.println("rest < 2  || rest < 0 ---  rest: " + rest);
			 throw new Exception("Imposible");
		 }
		 
		 return rest;
		 
	 }
}
