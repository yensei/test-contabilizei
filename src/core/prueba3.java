package core;


/*
 * 	
You have to organize a chess tournament in which players will compete head-to-head.

Here is how we proceed to form the duels: select a first player randomly, then, select his opponent at random among the remaining participants. The pair of competitors obtained forms one of the duels of the tournament. We proceed in the same manner to form all the other pairs.

In this exercise, we would like to know how many pairs it is possible to form knowing that the order of opponents in a pair does not matter.

For example, with 4 players named A, B, C and D, it is possible to get 6 different pairs : AB, AC, AD, BC, BD, CD.

Implement count to return the number of possible pairs. Parameter n corresponds to the number of players.

Try to optimize your solution so that, ideally, the duration of treatment is the same for any n.

Input: 2 <= n <= 10000
 */

public class prueba3 {
	
	public static void main(String[] args) {
		System.out.println(prueba3.count(4)); // 6
		System.out.println(prueba3.count(10000)); // 49995000
	}
	
	static int count(int n) {
		int contador = 0;
		int[] a= new int[n];
		
		while(n > 1){
			n -= 1;
			contador+= n;
			
		}
		
		return contador;
        
    }

}
