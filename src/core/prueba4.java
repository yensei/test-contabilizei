package core;

/*
 * Try to improve the code displayed in the answer editor by keeping the current behavior of the program.
 */
public class prueba4 {


	public static void main(String[] args) {

		Dog sammy = new Dog("Sammy");
		Cat smokey = new Cat("Smokey");
		
		System.out.println(Application.getAnimalName(sammy));
		System.out.println(Application.getAnimalName(smokey));
		
	}
	

}

abstract class Animal { }

class Dog extends Animal {
    String name;

    Dog(String name) {
        this.name = name;
    }

    String getName() {
        return name;
    }
}

class Cat extends Animal {
    String name;

    Cat(String name) {
        this.name = name;
    }

    String getName() {
        return name;
    }
}

class Application {

    static String getAnimalName(Animal a) {
    	
    	return a.getClass().getName();
    	/*
    	
        String name = null;
        if (a instanceof Dog) {
            name = ((Dog) a).getName();
        } else if (a instanceof Cat) {
            name = ((Cat) a).getName();
        }

        return name;
        */
    }
}
