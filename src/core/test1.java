package core;

public class test1 {

	void a(Service s, Connection c) throws Exception {
		s.setConnection(c);
		try {
			s.execute();
			c.commit();
		} catch (Exception e) {
			c.rollback();
			throw e;
		}finally {
			c.close();
		}
	}

}

interface Service {
	void execute() throws Exception;
	void setConnection(Connection c);
}

interface Connection {
	void commit();
	void rollback();
	void close();
}